//
//  User.swift
//  inventory
//
//  Created by Steven Capleton on 3/25/21.
//

import Foundation

class UserDebug {
    var username: String = ""
    var password: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var callories: Double = 0.0
    var weight: Double = 0.0
}
