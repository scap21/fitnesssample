//
//  FoodTableCell.swift
//  inventory
//
//  Created by Steven Capleton on 3/25/21.
//

import UIKit

class FoodTableCell: UITableViewCell {
    @IBOutlet var foodName: UILabel!
    @IBOutlet var calories: UILabel!
}
