//
//  FoodsViewController.swift
//  inventory
//
//  Created by Steven Capleton on 3/15/21.
//

import UIKit
import CoreData

class FoodsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var caloriesCount: UILabel!
    @IBOutlet var foodTableView: UITableView!
    
    var parentVC: MainViewController!
    var foods: [Food] = [Food]()
    var selectedIndex: Int!
    var totalCalories: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        caloriesCount.text = totalCalories.description
        
        foodTableView.delegate = self
        foodTableView.dataSource = self
    }
    
    @IBAction func addFoodAction(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Food", in: managedContext)!
        
        let item = NSManagedObject(entity: entity, insertInto: managedContext) as! Food
        item.name = "Name"
        item.calories = 0
        
        do {
            try managedContext.save()
            foods.append(item)
            foodTableView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell", for: indexPath) as! FoodTableCell
        cell.selectionStyle = .none
        cell.foodName.text = foods[indexPath.row].name
        cell.calories.text = foods[indexPath.row].calories.description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        createAlert()
    }
    
    func createAlert() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "ItemAlertViewController") as! ItemAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
    }

}
