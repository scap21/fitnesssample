//
//  SignUpViewController.swift
//  inventory
//
//  Created by Steven Capleton on 3/18/21.
//

import UIKit
import CoreData

class SignUpViewController: UIViewController {
    
    @IBOutlet var usernameError: UILabel!
    @IBOutlet var passwordError: UILabel!
    
    @IBOutlet var usernameText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var passwordConfirm: UITextField!
    
    var parentVC: LoginViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func handleCreateAccount(_ sender: Any) {
        var flag: Bool = false
        //Check if username is already in database
        if parentVC.users.count > 0 {
            for user in parentVC.users {
                if usernameText.text?.elementsEqual(user.username!) == true {
                    usernameError.isHidden = false
                    flag = true
                }
            }
        }
        
        //Check if passwords match
        if passwordText.text?.elementsEqual(passwordConfirm.text!) == false {
            flag = true
        }
        
        if flag == false {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
            
            let user = NSManagedObject(entity: entity, insertInto: managedContext) as! User
            user.username = usernameText.text
            user.password = passwordText.text
            
            do {
                try managedContext.save()
                parentVC.users.append(user)
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        performSegue(withIdentifier: "gotoMain", sender: sender)
    }
    

}
