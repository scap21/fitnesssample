//
//  ItemAlertViewController.swift
//  inventory
//
//  Created by Steven Capleton on 3/16/21.
//

import UIKit

class ItemAlertViewController: UIViewController {
    
    var parentVC: FoodsViewController!
    @IBOutlet var alertView: UIView!
    @IBOutlet var foodDetailText: UITextField!
    @IBOutlet var calsDetailText: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editView()
    }
    
    
    @IBAction func save(_ sender: Any) {
        parentVC.foods[parentVC.selectedIndex].name = foodDetailText.text ?? ""
        parentVC.foods[parentVC.selectedIndex].calories = Double( calsDetailText.text ?? "0") ?? 0
        if parentVC.foods[parentVC.selectedIndex].calories > parentVC.totalCalories{
            parentVC.caloriesCount.text = (parentVC.foods[parentVC.selectedIndex].calories - parentVC.totalCalories).description
        } else {
            parentVC.caloriesCount.text = (parentVC.totalCalories - parentVC.foods[parentVC.selectedIndex].calories).description
        }
        parentVC.foodTableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    func editView() {
        alertView.layer.cornerRadius = 15.0
        alertView.layer.borderWidth = 1.0
        alertView.layer.borderColor = UIColor.systemBlue.cgColor
        foodDetailText.becomeFirstResponder()
    }
}
