//
//  LoginViewController.swift
//  inventory
//
//  Created by Steven Capleton on 3/18/21.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {
    
    @IBOutlet var loginError: UILabel!
    @IBOutlet var usernameEditText: UITextField!
    @IBOutlet var passwordEditText: UITextField!
    
    var users: [User] = [User]()
    var parentVC: MainViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginError.isHidden = true
        getUserDb()
        
    }
    
    @IBAction func loginHandle(_ sender: Any) {
        if (users.count == 0) {
            loginError.isHidden = false
        }
        
        for u in users {
            if ((u.username?.elementsEqual(usernameEditText.text ?? "") == true) &&
                    (u.password?.elementsEqual(passwordEditText.text ?? "") == true) &&
                    (users.count != 0)){
                loginError.text = "Login button pressed"
            } else {
                loginError.isHidden = false
            }
        }
    }
    
    @IBAction func createAccountHandle(_ sender: Any) {
        let destVC = storyboard?.instantiateViewController(identifier: "SignUpViewController") as? SignUpViewController
        destVC?.parentVC = self
        performSegue(withIdentifier: "gotoSignUp", sender: sender)
    }
    
    func getUserDb() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        
        do {
            users = try managedContext.fetch(fetchRequest) as? [User] ?? []
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
