//
//  MainViewController.swift
//  inventory
//
//  Created by Steven Capleton on 3/15/21.
//

import UIKit

class MainViewController: UIViewController {
    
    //Main View Controller Buttons
    //The outlets allow for the appearance of the buttons to be modified
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var exercisesButton: UIButton!
    @IBOutlet var foodsButton: UIButton!
    @IBOutlet var accountButton: UIButton!
    
    //Greetings Label
    @IBOutlet var greetingsLbl: UILabel!
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        greetingsLbl.text = "Hello, \(user?.username ?? "please login")"
        
        // if user is logged out account button says login; else button says log out
        if user == nil {
            accountButton.setTitle("Login", for: .normal)
            login()
        } else {
            accountButton.setTitle("Log Out", for: .normal)
        }
        
        editButtons()
    }
    
    func login() {
        let loginVC = storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        loginVC?.parentVC = self
        self.navigationController?.pushViewController(loginVC!, animated: true)
    }
    
    
    //Account button is pressed
    @IBAction func accountHandle(_ sender: Any) {
        if user != nil {
            user = nil
        } else {
            login()
        }
        
    }
    
    //Profile button is pressed
    @IBAction func profileButtonHandle(_ sender: Any) {
        performSegue(withIdentifier: "gotoProfileVC", sender: sender)
    }
    
    
    //Exercise button is pressed
    @IBAction func exercisesButtonHandle(_ sender: Any) {
        performSegue(withIdentifier: "gotoExercisesVC", sender: sender)
    }
    
    
    //Foods button is pressed
    @IBAction func foodsButtonHandle(_ sender: Any) {
        performSegue(withIdentifier: "gotoFoodsVC", sender: sender)
    }
    
    
    
    
    
    //Round the corners of the buttons
    func editButtons() {
        profileButton.layer.cornerRadius = 10.0
        exercisesButton.layer.cornerRadius = 10.0
        foodsButton.layer.cornerRadius = 10.0
    }
}
